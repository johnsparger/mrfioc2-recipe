#!/bin/bash


LIBVERSION=${PKG_VERSION}

# This is the directory that the ESS-specific scripts are extracted into
export ESS_MRFIOC2_DIR=e3wrap
echo "INFO: Build prefix"
echo "${BUILD_PREFIX}"
echo "${EPICS_MODULES}"
echo "${LIBVERSION}"
echo "${PRJ}"

# Clean between variants builds
make -f Makefile.E3 clean
echo "INFO: Clean returns $?"

#Copy a new the substitution file patch 
#patch -d e3wrap -p1 <patch/evr-pcie-300dc.substitutions.patch.patch
cp patch/evr-pcie-300dc.substitutions.patch e3wrap/patch/evr-pcie-300dc.substitutions.patch

for i in e3wrap/patch/*.patch; 
do patch -p1 < $i; 
done

patch -d e3wrap -p1  <patch/conf.yaml.patch

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
echo "INFO: Build returns $?"

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
echo "INFO: Database returns $?"

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install
echo "INFO: Install returns $?"
